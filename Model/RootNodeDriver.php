<?php

namespace Bobbie\XSD_to_PHP\Model;

use Metadata\ClassMetadata;
use Metadata\Driver\DriverInterface;

class RootNodeDriver implements DriverInterface
{
    protected DriverInterface $delegate;
    protected ?array $rootNodeMaps;

    public function __construct(DriverInterface $delegate, ?array $rootNodeMaps)
    {
        $this->delegate = $delegate;
        $this->rootNodeMaps = $rootNodeMaps;
    }

    public function loadMetadataForClass(\ReflectionClass $class): ?ClassMetadata
    {
        $result = $this->delegate->loadMetadataForClass($class);
        if ($this->rootNodeMaps != null) {
            foreach ($this->rootNodeMaps as $prefix => $uri) {
                $result->registerNamespace($uri, $prefix);
            }
            $this->rootNodeMaps = null;
        }
        return $result;
    }
}
