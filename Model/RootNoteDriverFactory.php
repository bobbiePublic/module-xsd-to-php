<?php


namespace Bobbie\XSD_to_PHP\Model;

use Doctrine\Common\Annotations\Reader;
use JMS\Serializer\Builder\DefaultDriverFactory;
use JMS\Serializer\Builder\DriverFactoryInterface;
use JMS\Serializer\Expression\CompilableExpressionEvaluatorInterface;
use JMS\Serializer\Naming\PropertyNamingStrategyInterface;
use JMS\Serializer\Type\ParserInterface;
use Metadata\Driver\DriverInterface;

class RootNoteDriverFactory implements DriverFactoryInterface
{

    protected DriverFactoryInterface $delegate;
    protected ?array $rootNodeMaps;

    public function __construct(
        PropertyNamingStrategyInterface $propertyNamingStrategy,
        ?array $rootNodeMaps,
        ?ParserInterface $typeParser = null,
        ?CompilableExpressionEvaluatorInterface $expressionEvaluator = null
    ) {
        $this->delegate = new DefaultDriverFactory($propertyNamingStrategy, $typeParser, $expressionEvaluator);
        $this->rootNodeMaps = $rootNodeMaps;
    }

    public function createDriver(array $metadataDirs, ?Reader $annotationReader = null): DriverInterface
    {
        return new RootNodeDriver($this->delegate->createDriver($metadataDirs, $annotationReader), $this->rootNodeMaps);
    }
}
