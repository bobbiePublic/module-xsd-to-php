<?php

namespace Bobbie\XSD_to_PHP\Helper;

use Bobbie\XSD_to_PHP\Model\RootNoteDriverFactory;
use GoetasWebservices\Xsd\XsdToPhpRuntime\Jms\Handler\BaseTypesHandler;
use GoetasWebservices\Xsd\XsdToPhpRuntime\Jms\Handler\XmlSchemaDateHandler;
use JMS\Serializer\Handler\HandlerRegistryInterface;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use JMS\Serializer\Serializer as JMSSerializer;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Type\Exception\InvalidNode;

abstract class AbstractSerializer
{
    protected function build(array $mapping, array $rootNodeMaps = []) : JMSSerializer
    {
        $serializerBuilder = SerializerBuilder::create();

        $serializerBuilder->addMetadataDirs($mapping);
        $namingStrategy = new IdenticalPropertyNamingStrategy();
        $serializerBuilder->setPropertyNamingStrategy($namingStrategy);
        $serializerBuilder->configureHandlers(function (HandlerRegistryInterface $handler) use ($serializerBuilder) {
            $serializerBuilder->addDefaultHandlers();
            $handler->registerSubscribingHandler(new BaseTypesHandler());
            $handler->registerSubscribingHandler(new XmlSchemaDateHandler());
        });
        $serializerBuilder->setMetadataDriverFactory(new RootNoteDriverFactory(
            $namingStrategy,
            $rootNodeMaps,
            null,
            null
        ));
        return $serializerBuilder->build();
    }

    abstract public function getSerializer() : JMSSerializer;

    public function serializeXML($data) : string
    {
        return $this->getSerializer()->serialize($data, 'xml');
    }

    public function deSerializeXML(string $data)
    {
        $rootNode = $this->inferRootNode($data);
        if ($rootNode !== null) {
            return $this->getSerializer()->deserialize($data, $rootNode, 'xml');
        } else {
            throw new InvalidNode('Cannot infer root node type');
        }
    }

    public function getRootNodeName(string $dataXML)
    {
        return simplexml_load_string($dataXML)->getName();
    }

    abstract protected function getRootNodeMap() : array;

    public function inferRootNode(string $dataXML)
    {
        $xmlName = $this->getRootNodeName($dataXML);
        if (isset($this->getRootNodeMap()[$xmlName])) {
            return $this->getRootNodeMap()[$xmlName];
        } else {
            return null;
        }
    }
}
